FROM node:12.22-buster
WORKDIR /opt/dockerApp
COPY package.json /opt/dockerApp
RUN npm install
COPY . /opt/dockerApp
EXPOSE 3000
CMD ["npm","run","start"]